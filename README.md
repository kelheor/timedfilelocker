# Timed File Locker #

This small utility will allow you to seal specified file until a certain date.

Password will be used to encrypt/decrypt randomly created encryption keys with AES-256. And with those keys, file can be encrypted/decrypted by using RSA/AES256/ECDSA/HMAC by the following algorithm:  
File will be encrypted with AES256(PBE WITH SHA256 AND 256 BIT AES-CBC-BC). Random AES256 key will be generated (SHA1PRNG, 512 bytes) and encrypted by RSA encryption(key size up to 8192). File also will be signed with ECDSA (SHA384/ECDSA, prime192v1) to check if data was changed. Message authentication code (HMAC-SHA512) also will be generated to check if data was changed.

The current time will be requested from pool.ntp.org NTP server.

**Arguments format:** filename operation(lock/unlock) password time(number+s/m/h/d/w/y), where s - seconds, m - minutes, h - hours, d - days, w - weeks, y - years.

### **How to build:** ###
Use your IDE or maven, or open console in project folder and enter "mvnw clean install", then use timed-file-locker-1.0-jar-with-dependencies.jar from target folder or from your .m2 repository folder.

### **Usage example:** ###
**Lock file for 1 minute:** java -jar timed-file-locker-1.0-jar-with-dependencies.jar "YourApplication.exe" lock 12345 1m  
**Lock file for 1 hour:** java -jar timed-file-locker-1.0-jar-with-dependencies.jar "YourApplication.exe" lock 12345 1h  
**Unlock file:** java -jar timed-file-locker-1.0-jar-with-dependencies.jar "YourApplication.exe" unlock 12345

### **How to install OpenJDK 11 on Windows** ###

- Download OpenJDK 11 (https://jdk.java.net/11/)
- Extract the zip file and copy **jdk-11.0.1** folder into **"C:\Program Files\Java\"**
- Select Control Panel and then System.
- Click Advanced and then Environment Variables.
- Add the location of the bin folder of the JDK installation to the PATH variable in System Variables (**"C:\Program Files\Java\jdk-11.0.1\bin"**).
- Under System Variables, click New.
- Enter the variable name as JAVA_HOME.
- Enter the variable value as the installation path of the JDK (**"C:\Program Files\Java\jdk-11.0.1"**).
- Click OK.
- Click Apply Changes.
- Configure the JDK in your IDE.
- To see if it worked, open up the Command Prompt and type **java -version** and see if it prints your newly installed JDK.

### **How to install OpenJDK 11 on Mac** ###

- Download OpenJDK 11 (https://jdk.java.net/11/)
- Open terminal
- cd ~/Downloads
- tar xf openjdk-11.0.1_osx-x64_bin.tar
- sudo mv jdk-11.0.1.jdk /Library/Java/JavaVirtualMachines/
- java -version to check if it installed correctly

### **Potential vulnerabilities** ###
Although it's impossible to decrypt files without a valid password, it's possible to hack time interval checks.  
First and simplest way is to remove time check from source code, then compile and use the new app to unseal file, if you know the password. To protect from this to a certain point, you can make a strong and unique bytes delimiters and compile the program with them. Then, even if somebody will try to change the code and make a version without time check, it won't work without proper bytes delimiter. Hacker still can analyze the sealed file and try to find delimiters. To protect from this, you can modify the code and use multiple unique delimiters instead of single universal.  
Another possible vulnerable is to do MITM attack and fake current time response from the NTP server.