package com.academy.tfl;

import com.academy.tfl.api.encryption.algorithms.asymmetric.AsymmetricEncryptionAlgorithm;
import com.academy.tfl.api.encryption.algorithms.asymmetric.rsa.RSA;
import com.academy.tfl.api.encryption.algorithms.asymmetric.rsa.RSAKeyPair;
import com.academy.tfl.api.encryption.algorithms.dsa.DigitalSignatureAlgorithm;
import com.academy.tfl.api.encryption.algorithms.dsa.ecdsa.ECDSA;
import com.academy.tfl.api.encryption.algorithms.dsa.ecdsa.ECDSAKeyPair;
import com.academy.tfl.api.encryption.algorithms.mac.MessageAuthenticationCodeAlgorithm;
import com.academy.tfl.api.encryption.algorithms.mac.hmac.HMAC;
import com.academy.tfl.api.encryption.algorithms.mac.hmac.MACSecretKey;
import com.academy.tfl.api.encryption.algorithms.symmetric.SymmetricEncryptionAlgorithm;
import com.academy.tfl.api.encryption.algorithms.symmetric.aes.AES256;
import com.academy.tfl.api.secured.data.EncryptionKeys;
import com.academy.tfl.api.secured.data.SecuredData;
import com.academy.tfl.utils.TransferUtil;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.Security;
import java.time.LocalDate;
import java.util.*;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    private static String WRONG_ARGUMENTS_ERROR = "Wrong input arguments. Require format: filename operation(lock,unlock) password time(number+s/m/h/d/w/y). Example: \"G:\\[Work]\\Test.txt\" lock 12345 1m";

    public static void main(String[] args) {

        // Check filename and password and if we have at least 3 arguments
        if (args.length < 3 || args[0] == null || args[0].isEmpty() || args[2] == null || args[2].isEmpty()) {
            LOG.error(WRONG_ARGUMENTS_ERROR);
            return;
        }

        // Lock or unlock file
        if (args[1].equals("lock")) {

            // Check if we have time interval setting
            if (args.length < 4 || args[3] == null || args[3].isEmpty()) {
                LOG.error(WRONG_ARGUMENTS_ERROR);
                return;
            }

            // Lock file
            lockFile(args[0], args[3], args[2]);

        } else if (args[1].equals("unlock")) {

            // Unlock file
            unlockFile(args[0], args[2]);

        } else {
            // Unknown operation specified
            LOG.error(WRONG_ARGUMENTS_ERROR);
            return;
        }

    }

    /**
     * Lock file
     *
     * @param filepath Path to file
     * @param timeInterval Time interval to block file until it ends
     */
    @SuppressWarnings("Duplicates")
    private static void lockFile(String filepath, String timeInterval, String password) {
        try {

            // Read file
            Path path = Paths.get(filepath);
            byte[] file = Files.readAllBytes(path);

            // Init BouncyCastleProvider
            if (Security.getProvider("BC") == null) {
                Security.addProvider(new BouncyCastleProvider());
            }

            // Calculate password hash
            MessageDigest mda = MessageDigest.getInstance("SHA-512", "BC");
            byte[] passwordHash = mda.digest(password.getBytes());

            // Making keys
            RSAKeyPair rsaKeyPair = new RSAKeyPair(2048);
            ECDSAKeyPair ecdsaKeyPair = new ECDSAKeyPair();
            MACSecretKey macSecretKey = new MACSecretKey();

            // Encrypt keys with password via AES-256, then decrypt and use in wallet data decryption
            EncryptionKeys encryptionKeys = new EncryptionKeys();
            encryptionKeys.setAeaPrivateKey(rsaKeyPair.getPrivateKey());
            encryptionKeys.setAeaPublicKey(rsaKeyPair.getPublicKey());
            encryptionKeys.setDsaPrivateKey(ecdsaKeyPair.getPrivateKey());
            encryptionKeys.setDsaPublicKey(ecdsaKeyPair.getPublicKey());
            encryptionKeys.setMacSecretKey(macSecretKey.getSecretKey());

            SymmetricEncryptionAlgorithm keysEncryptor = new AES256();

            // Encrypt keys - can be stored in database and decrypted by user password
            byte[] encryptedKeys = keysEncryptor.encrypt(TransferUtil.serializeAndCompress(encryptionKeys), password);

            // AES256 RSA2048 ECDSA HMAC
            SymmetricEncryptionAlgorithm symmetricEncryptionAlgorithm = new AES256();
            AsymmetricEncryptionAlgorithm asymmetricEncryptionAlgorithm = new RSA();
            DigitalSignatureAlgorithm digitalSignatureAlgorithm = new ECDSA();
            MessageAuthenticationCodeAlgorithm messageAuthenticationCodeAlgorithm = new HMAC();

            // Data to encrypt
            byte[] data = TransferUtil.compress(file);

            // Encrypt data
            SecuredData securedData = new SecuredData(data);
            securedData.encrypt(encryptionKeys.getAeaPublicKey(), encryptionKeys.getDsaPrivateKey(), encryptionKeys.getMacSecretKey(), symmetricEncryptionAlgorithm, asymmetricEncryptionAlgorithm, digitalSignatureAlgorithm, messageAuthenticationCodeAlgorithm);

            // Serialize
            byte[] encryptedData = TransferUtil.serializeAndCompress(securedData);

            // Receive current time from server
            Date currentTime = getCurrentTime();

            // Add specified time interval
            Date unlockDate = currentTime;
            Integer timeIntervalNumber = Integer.parseInt(timeInterval.substring(0, timeInterval.length()-1));
            switch (timeInterval.substring(timeInterval.length()-1)) {
                case "s": {
                    unlockDate = DateUtils.addSeconds(currentTime, timeIntervalNumber);
                    break;
                }
                case "m": {
                    unlockDate = DateUtils.addMinutes(currentTime, timeIntervalNumber);
                    break;
                }
                case "h": {
                    unlockDate = DateUtils.addHours(currentTime, timeIntervalNumber);
                    break;
                }
                case "d": {
                    unlockDate = DateUtils.addDays(currentTime, timeIntervalNumber);
                    break;
                }
                case "w": {
                    unlockDate = DateUtils.addWeeks(currentTime, timeIntervalNumber);
                    break;
                }
                case "y": {
                    unlockDate = DateUtils.addYears(currentTime, timeIntervalNumber);
                    break;
                }
            }

            // Encrypt unlock date
            byte[] unlockDateByteArray = TransferUtil.serializeAndCompress(unlockDate);
            SecuredData unlockDateSecuredData = new SecuredData(unlockDateByteArray);
            unlockDateSecuredData.encrypt(encryptionKeys.getAeaPublicKey(), encryptionKeys.getDsaPrivateKey(), encryptionKeys.getMacSecretKey(), symmetricEncryptionAlgorithm, asymmetricEncryptionAlgorithm, digitalSignatureAlgorithm, messageAuthenticationCodeAlgorithm);
            byte[] encryptedUnlockDate = TransferUtil.serializeAndCompress(unlockDateSecuredData);

            // Sealed file data
            byte[] delimiter = { (byte) 0x73, (byte) 0x13, (byte) 0x86, (byte) 0x81, (byte) 0x98};
            byte[] sealedFile = ByteBuffer.allocate(encryptedData.length + delimiter.length + encryptedKeys.length + delimiter.length + passwordHash.length + delimiter.length + encryptedUnlockDate.length).put(encryptedData).put(delimiter).put(encryptedKeys).put(delimiter).put(passwordHash).put(delimiter).put(encryptedUnlockDate).array();

            Path newPath = Paths.get(filepath + ".sealed");
            if(Files.exists(newPath)) {
                Files.delete(newPath);
            }

            Files.write(newPath, sealedFile, CREATE_NEW);

            // Remove original file
            Files.delete(path);

            LOG.info("File successfully locked");

        } catch (Exception e) {
            LOG.error("Error during locking file: " + e.getMessage());
        }
    }

    /**
     * Unlock file
     *
     * @param filepath Path to file
     */
    @SuppressWarnings("Duplicates")
    private static void unlockFile(String filepath, String password) {
        try {

            // Read file, read time interval, check interval

            Path path = Paths.get(filepath);
            byte[] data = Files.readAllBytes(path);
            byte[] delimiter = { (byte) 0x73, (byte) 0x13, (byte) 0x86, (byte) 0x81, (byte) 0x98};
            List<byte[]> dataList = tokens(data, delimiter);

            byte[] encryptedData = dataList.get(0);
            byte[] encryptedKeys = dataList.get(1);
            byte[] storedPasswordHash = dataList.get(2);
            byte[] encryptedUnlockDate = dataList.get(3);

            // Init BouncyCastleProvider
            if (Security.getProvider("BC") == null) {
                Security.addProvider(new BouncyCastleProvider());
            }

            // Calculate password hash
            MessageDigest mda = MessageDigest.getInstance("SHA-512", "BC");
            byte[] passwordHash = mda.digest(password.getBytes());

            // Throw error if password is invalid
            if (!Arrays.equals(storedPasswordHash,passwordHash)) {
                throw new Exception("Invalid password. Can't decrypt file.");
            }

            // Decrypt encryption keys
            SymmetricEncryptionAlgorithm keysEncryptor = new AES256();
            EncryptionKeys decryptedKeys = (EncryptionKeys) TransferUtil.decompressAndDecerialize(keysEncryptor.decrypt(encryptedKeys, password));

            // AES256 RSA2048 ECDSA HMAC
            SymmetricEncryptionAlgorithm symmetricEncryptionAlgorithm = new AES256();
            AsymmetricEncryptionAlgorithm asymmetricEncryptionAlgorithm = new RSA();
            DigitalSignatureAlgorithm digitalSignatureAlgorithm = new ECDSA();
            MessageAuthenticationCodeAlgorithm messageAuthenticationCodeAlgorithm = new HMAC();

            // Receive current time from server
            Date currentTime = getCurrentTime();

            // Decrypt time interval
            SecuredData unlockDateSecuredData = (SecuredData) TransferUtil.decompressAndDecerialize(encryptedUnlockDate);
            unlockDateSecuredData.decrypt(decryptedKeys.getAeaPrivateKey(), decryptedKeys.getDsaPublicKey(), decryptedKeys.getMacSecretKey());
            Date unlockDate = (Date) TransferUtil.decompressAndDecerialize(unlockDateSecuredData.getData());

            LOG.info("Unlock date: " + unlockDate);

            // Compare time
            if(!currentTime.after(unlockDate)) {
                throw new Exception("Specified time interval has not yet passed. Can't unlock file.");
            }

            // Decrypt original file data
            SecuredData securedData = (SecuredData) TransferUtil.decompressAndDecerialize(encryptedData);
            securedData.decrypt(decryptedKeys.getAeaPrivateKey(), decryptedKeys.getDsaPublicKey(), decryptedKeys.getMacSecretKey());
            byte[] originalFileData = TransferUtil.decompress(securedData.getData());

            String newFilePath = filepath.substring(0, filepath.length() - 7);
            Path newPath = Paths.get(newFilePath);

            if(Files.exists(newPath)) {
                Files.delete(newPath);
            }

            Files.write(newPath, originalFileData, CREATE_NEW);

            // Remove original file
            Files.delete(path);

            LOG.info("File successfully unlocked");

        } catch (Exception e) {
            LOG.error("Error during unlocking file: " + e.getMessage());
        }
    }

    public static List<byte[]> tokens(byte[] array, byte[] delimiter) {
        List<byte[]> byteArrays = new LinkedList<>();
        if (delimiter.length == 0) {
            return byteArrays;
        }
        int begin = 0;

        outer:
        for (int i = 0; i < array.length - delimiter.length + 1; i++) {
            for (int j = 0; j < delimiter.length; j++) {
                if (array[i + j] != delimiter[j]) {
                    continue outer;
                }
            }
            byteArrays.add(Arrays.copyOfRange(array, begin, i));
            begin = i + delimiter.length;
        }
        byteArrays.add(Arrays.copyOfRange(array, begin, array.length));
        return byteArrays;
    }

    public static Date getCurrentTime() throws Exception {
        String TIME_SERVER = "pool.ntp.org";
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
        TimeInfo timeInfo = timeClient.getTime(inetAddress);
        long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
        Date currentTime = new Date(returnTime);
        return currentTime;
    }



}
